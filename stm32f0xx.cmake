set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

# Optimisation & Debug
set(OPT_LEVEL -Og)
set(DBG_LEVEL -g3)

# Toolchain
set(CMAKE_C_COMPILER   arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-as)
set(CMAKE_OBJCOPY      arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP      arm-none-eabi-objdump)
set(CMAKE_SIZE         arm-none-eabi-size)
set(CMAKE_READELF      arm-none-eabi-readelf)

# Flags
set(CMAKE_EXE_LINKER_FLAGS "-mthumb -mcpu=cortex-m0 --specs=nano.specs --specs=nosys.specs -Wl,--gc-sections -Wl,-Map -Wl,${CMAKE_PROJECT_NAME}.map" CACHE INTERNAL "exe link flags")
set(CMAKE_C_FLAGS   "-mthumb -mcpu=cortex-m0 -fno-builtin ${OPT_LEVEL} ${DBG_LEVEL} -Wall -std=gnu99" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS "-mthumb -mcpu=cortex-m0 -fno-builtin ${OPT_LEVEL} ${DBG_LEVEL} -Wall" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS "-mthumb -mcpu=cortex-m0" CACHE INTERNAL "asm compiler flags")
