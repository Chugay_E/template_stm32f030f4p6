/**
 * @file		main.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		15.09.2020
 * @brief
 */

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */
#define IWDG_RESET		(IWDG->KR = 0xAAAA)		//	Reset watchdog

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*-------------------------------------------------------- Private variables: */
uint16_t ms_cnt = 0;

/*---------------------------------------------- Private funcrion prototypes: */
void RCC_init(void);
void IWDG_init(uint16_t wt);
void SysTick_init(void);
//void PA6_init (void);

/*----------------------------------------------------------------- Function: */
int main (void)
{
    RCC_init();
    IWDG_init(1000);
    SysTick_init();
//  PA6_init();

    RCC->CR  |= RCC_CR_CSSON;		//	start clock secure
    RCC->CIR |= RCC_CIR_HSERDYIE;	//	enable interrupt if HSE ready

    while (1)
    {
        IWDG_RESET;

    }
}


/*-------------------------------------------- System initialization function */
void RCC_init(void)
{
    RCC->CR   &= ~(RCC_CR_HSEON | RCC_CR_HSEBYP | RCC_CR_CSSON | RCC_CR_PLLON);
    RCC->CFGR &= ~(RCC_CFGR_SW | RCC_CFGR_HPRE | RCC_CFGR_PPRE | RCC_CFGR_ADCPRE |
                   RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMUL |
                   RCC_CFGR_MCO | RCC_CFGR_MCOPRE | RCC_CFGR_PLLNODIV);
    RCC->CFGR2 = RCC_CFGR2_PREDIV_DIV1;

    //	Start HSE, wait ready
    uint32_t start_up_cnt = 0;
    RCC->CR |= RCC_CR_HSEON;
    do {
        start_up_cnt++;
    } while(((RCC->CR & RCC_CR_HSERDY) == 0) && (start_up_cnt != 4000));
//	RCC->CR |= RCC_CR_CSSON;		//	start clock secure
//	RCC->CIR |= RCC_CIR_HSERDYIE;	//	enable interrupt if HSE ready

    //	FLASH buffer configure
    FLASH->ACR |= FLASH_ACR_LATENCY;			//	1 cycle latency

    //	Select PLL source:
    if ((RCC->CR & RCC_CR_HSERDY) != 0) {
        RCC->CFGR |= RCC_CFGR_PLLMUL6;			//	HSE * 6
        RCC->CFGR |= RCC_CFGR_PLLSRC_HSE_PREDIV;
        RCC->CIR |= RCC_CIR_HSERDYC;			//	if HSE restarted
    } else {
        RCC->CFGR |= RCC_CFGR_PLLMUL12;			//	HSI/2 * 12
        RCC->CFGR &= ~RCC_CFGR_PLLSRC;			//	HSI/2 source for PLL
    }

    //	Start PLL with selected SYSCLK source
    RCC->CR |= RCC_CR_PLLON;
    while(!(RCC->CR & RCC_CR_PLLRDY));
    RCC->CFGR |= RCC_CFGR_SW_PLL;
    while((RCC->CFGR & RCC_CFGR_SWS) != 0x00000008);
    __enable_irq();
}

void IWDG_init(uint16_t wt)
{
    IWDG->KR = 0x5555;			//	Key for unlock timer register
    IWDG->PR = 7;				//	Clock divider 256 (40kHz/256)
    IWDG->RLR = (wt*40)>>8;		//	Reload register update
    IWDG->KR = 0xAAAA;			//	Reload
    IWDG->KR = 0xCCCC;			//	Start
}

void SysTick_init(void)
{
    SysTick->LOAD = 47999;
    SysTick->VAL  = 47999;
//	NVIC_SetPriority(SysTick_IRQn, 7);
    SysTick->CTRL =(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk);
}

/*
void PA6_init (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

    GPIOA->MODER   &= ~GPIO_MODER_MODER6;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_6;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR6;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER6_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR6;	//	high speed
}
*/
/*-------------------------------------------------- System interrupt handler */
void NMI_Handler(void)
{
    if (RCC->CIR & RCC_CIR_CSSF) {			//	HSE failure
        RCC->CIR |= RCC_CIR_CSSC;
        RCC_init();
        while(RCC->CIR & RCC_CIR_CSSF);
    }
    if (RCC->CIR & RCC_CIR_HSERDYF) {		//	HSE restarted
        RCC->CIR |= RCC_CIR_HSERDYC;
        RCC_init();
        while(RCC->CIR & RCC_CIR_HSERDYF);
    }
}

void SysTick_Handler(void)
{
    if (ms_cnt < 999) {
        (ms_cnt++);
    } else {
        (ms_cnt = 0);
    }

//  GPIOA->ODR ^= GPIO_ODR_6;
/*
    GPIOA->BSRR = GPIO_BSRR_BS_6;
    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");
    __asm__("nop"); __asm__("nop");                                     //  250 ns

    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");
    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");     //  500 ns

    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");
    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");     //  750 ns

    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");
    __asm__("nop"); __asm__("nop"); __asm__("nop"); __asm__("nop");     //  1 us
    GPIOA->BRR = GPIO_BRR_BR_6;
*/
}
